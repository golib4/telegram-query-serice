package telegram_query

import (
	"encoding/json"
	"errors"
	"fmt"
	telWebapp "github.com/telegram-mini-apps/init-data-golang"
	"net/url"
	"time"
)

type QueryService struct {
	params Params
}

func NewQueryService(params Params) *QueryService {
	return &QueryService{params: params}
}

type Params struct {
	BotToken                       string
	TelegramQueryLifetimeInSeconds int
}

func (v QueryService) Validate(query string) error {
	lifetime := time.Duration(v.params.TelegramQueryLifetimeInSeconds) * time.Second
	err := telWebapp.Validate(query, v.params.BotToken, lifetime)
	if err != nil {
		return errors.New("invalid telegram query provided")
	}

	return nil
}

type UserData struct {
	ID           int64  `json:"id"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Username     string `json:"username"`
	LanguageCode string `json:"language_code"`
}

func (v QueryService) Parse(query string) (*UserData, error) {
	values, err := url.ParseQuery(query)
	if err != nil {
		return nil, fmt.Errorf("can not parse query: %s", err)
	}

	decodedUserData := UserData{}
	userData := values.Get("user")
	err = json.Unmarshal([]byte(userData), &decodedUserData)
	if err != nil {
		return nil, fmt.Errorf("can not unmarshal data: %s", err)
	}

	return &UserData{
		ID:           decodedUserData.ID,
		FirstName:    decodedUserData.FirstName,
		LastName:     decodedUserData.LastName,
		Username:     decodedUserData.Username,
		LanguageCode: decodedUserData.LanguageCode,
	}, nil
}
